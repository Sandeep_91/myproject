<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Comment;
class AddCommentController extends Controller
{
    //
    
    function action(Request $request)
    {
        $postbody = $request->json()->all();
        $comment = $postbody['comment'];
        $post_id = $postbody['post_id'];
        $form_data = array(
           'comment' => $comment,
           'post_id' => $post_id
       );
         Comment::create($form_data);
         return response()->json([
           'status'   => "success" ,
           'message'   => "Comment Added Successfully" ,
           'class_name'  => 'alert-danger'
          ]);

    }
}
