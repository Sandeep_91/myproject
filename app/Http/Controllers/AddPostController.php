<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\post;
class AddPostController extends Controller
{
    //
     function index()
    {
         
     return view('ajax_upload');
    }

    function action(Request $request)
    {
   
     $validation = Validator::make($request->all(), [
      'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
     ]);
     if($validation->passes())
     {
         
      $image = $request->file('select_file');
      $video_url = $request->video_url;
    
      $new_name = rand() . '.' . $image->getClientOriginalExtension();
      $image->move(public_path('images'), $new_name);
       $form_data = array(
           'post_image_url' => $new_name,
           'video_url' => $video_url
       );
         post::create($form_data);
      return response()->json([
       'message'   => 'Image Upload Successfully',
       'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',
       'class_name'  => 'alert-success'
      ]);
     }
     else
     {
      return response()->json([
       'message'   => $validation->errors()->all(),
       'uploaded_image' => '',
       'class_name'  => 'alert-danger'
      ]);
     }
    }
    
    function get(){
          $posts = post::with('comments')->get();
          return response()->json($posts);
    }
    
    public function edit($id)
    {
        if(request()->ajax())
        {
            $data = post::findOrFail($id);
            return response()->json(['data' => $data]);
        }
    }
    
    public function update(Request $request)
    {
       
        $image = $request->file('image');
        
        if($image != '')
        {
          
            $rules = array(
                'video_link'    =>  'required',
                'image'         =>  'image|max:2048'
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
            $form_data = array(
                'video_url'       =>   $request->video_link,
                'post_image_url'            =>   $image_name
            );
            post::whereId($request->hidden_id)->update($form_data);
            
        }
        else
        {
            $url = $request->video_link;
            if($url != ""){
                $form_data = array(
                    'video_url'       =>   $request->video_link
                );
                 post::whereId($request->hidden_id)->update($form_data);
            }else{
                $rules = array(
                    'video_link'    =>  'required'
                );

                $error = Validator::make($request->all(), $rules);

                if($error->fails())
                {
                    return response()->json(['errors' => $error->errors()->all()]);
                }
            }
            
        }

        

        return response()->json(['success' => 'Data is successfully updated']);
    }

     public function destroy($id)
    {
        $data = post::findOrFail($id);
        $data->delete();
    }
    
}
