<?php

namespace App;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class post extends Model
{
    //
    
    protected $fillable = ['post_image_url','video_url'];
    
    public function comments(){
        return $this->hasMany('App\Comment','post_id','id');

//        return $this->hasMany(Comment::class);
    }
}
