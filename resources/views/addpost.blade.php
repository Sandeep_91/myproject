
<!DOCTYPE html>
<html>
 <head>
  <title>Add Post</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">Add Post</h3>
   <br />
   <div class="alert" id="message" style="display: none"></div>
   <form method="post" id="upload_form" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
     <table class="table">
      <tr>
       <td width="40%" align="right"><label>Select File for Upload</label></td>
       <td width="30"><input type="file" name="select_file" id="select_file" /></td>
       <td width="30%" align="left"><span class="text-muted">jpg, png, gif</span></td>
      </tr>
     
      <tr>
       <td width="40%" align="right"><label>Enter URL</label></td>
       <td width="30"><input type="text" name="video_url" id="video_url" /></td>
       <td width="30%" align="left"></td>
      </tr>
      <tr>
       <td width="40%" align="right"></td>
       <td width="30"><input type="submit" name="upload" id="upload" class="btn btn-primary" value="Upload"></td>
       <td width="30%" align="left"></td>
      </tr>
     </table>
    </div>
   </form>
   <br />
<!--   <span id="uploaded_image"></span>-->
  </div>
     <script>
        $(document).ready(function(){

         $('#upload_form').on('submit', function(event){
              event.preventDefault();
              $.ajax({
               url:"{{ route('addpost.action') }}",
               method:"POST",
               data:new FormData(this),
               dataType:'JSON',
               contentType: false,
               cache: false,
               processData: false,
               success:function(data)
               {
                $('#message').css('display', 'block');
                $('#message').html(data.message);
                $('#message').addClass(data.class_name);
//                $('#uploaded_image').html(data.uploaded_image);
               }
              })
             });

        });
        </script>
 </body>
</html>

