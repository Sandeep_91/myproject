
<!DOCTYPE html>
<html>
 <head>
  <title>View Post</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <style>
        div.show-image {
            position: relative;
            float:left;
            margin:5px;
        }
        div.show-image:hover img{
            opacity:0.5;
        }
        div.show-image:hover input {
            display: block;
        }
        div.show-image input {
            position:absolute;
            display:none;
        }
        div.show-image input.update {
            top:0;
            left:82%;
        }
        div.show-image input.delete {
            top:0;
            left:92%;
        }
     </style>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">View Post</h3>
    <div id="postssss">
        <div class="card" style="width: 25rem;">
          <img class="card-img-top" src="{{url('/images/602392669.png')}}" alt="Card image cap">
          <div class="card-body">
            <input type="text" name="comment" id="comment" placeholder="add comment" style="width: 75%;">
            <button type="button" id="addcomment" style="width: 20%;">Add</button>
          </div>
        </div>

      </div>
  
   
  </div>
     
<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="edit_form" class="form-horizontal" enctype="multipart/form-data">
            <div class="form-group">
            <label class="control-label col-md-4">Enter url : </label>
            <div class="col-md-8">
             <input type="text" name="video_link" id="video_link" />
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4">Select Image : </label>
            <div class="col-md-8">
             <input type="file" name="image" id="image" />
             <span id="store_image"></span>
            </div>
           </div>
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Edit" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>
     
<div class="modal fade" id="modalYT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
      <div class="modal-body mb-0 p-0">

        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/A3PDXmYoF5U" allowfullscreen></iframe>
        </div>

      </div>


    </div>
    <!--/.Content-->

  </div>
</div>
     <script>
        
         
         $(document).ready(function(){

          $.ajax({
           url:"{{ route('viewpost.get') }}",
           method:"GET",
           cache: false,
           success:function(data)
           {

            console.log(data);
               var html = "";
               for(var i=0; i<data.length;i++ )
               {
                 html += '<div class="card" style="width: 80%;"><div class="show-image"><img class="card-img-top" id="img-'+data[i].id+'" src="/images/'+data[i].post_image_url+'"  alt=""> <input class="update" type="button" value="Update" onclick="updatecomment('+data[i].id+')"/><input class="delete" type="button" value="Delete" onclick="deletecomment('+data[i].id+')" /></div><div class="card-body"><p onclick="openvideo(\''+data[i].video_url+'\')" id="video-link-'+data[i].id+'">'+data[i].video_url+'</p><input type="text" name="comment" id="comment-'+data[i].id+'" placeholder="add comment" style="width: 75%;"><button type="button" id="addcomment-'+data[i].id+'" class="addcomment" data-id="'+data[i].id+'" style="width: 20%;" onclick="addcomment('+data[i].id+')">Add</button></div></div><br><br>' ;
               }
       
          $("#postssss").html(html);
           }
         });

        });
         
         function addcomment(id){
              $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
             
             var cmt = $("#comment-"+id).val();
             var pagedata = {
                 "comment":cmt,
                 "post_id":id
             }
              $.ajax({
               url:"{{ route('addcomment.action') }}",
               method:"POST",
               data:JSON.stringify(pagedata),
               contentType: "application/json",
               cache: false,
               processData: false,
               success:function(data)
               {

               }
              })
           
         }
         
         function updatecomment(id){
               
                  var img = $("#img-"+id).attr('src');  
                  var url = $("#video-link-"+id).html();  
                  $('#store_image').html("<img src={{ URL::to('/') }}" + img + " width='70%' class='img-thumbnail' />");
                  $('#hidden_id').val(id);
                  $("#video_link").val(url);
                  $('#formModal').modal('show');
          
         }
         
          function deletecomment(id){
              if(confirm("Are you sure want to delete this??")){
                     $.ajax({
                           url:"viewpost/destroy/"+id,
                           success:function(data)
                           {
                                location.reload();
                           }
                          })
                 }
               
         }
         
         function openvideo(url){
              
              $('#modalYT').find('iframe').attr('src',url);
              $('#modalYT').modal('show');
         }
            $('#edit_form').on('submit', function(event){
                  event.preventDefault();
                  $.ajaxSetup({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                    });

                
                  $.ajax({
                   url:"{{ route('viewpost.update') }}",
                   method:"POST",
                   data:new FormData(this),
                   dataType:'JSON',
                   contentType: false,
                   cache: false,
                   processData: false,
                   success:function(data)
                   {
                     if(data.errors)
                         {
                          html = '<div class="alert alert-danger">';
                          for(var count = 0; count < data.errors.length; count++)
                          {
                           html += '<p>' + data.errors[count] + '</p>';
                          }
                          html += '</div>';
                             $('#form_result').html(html);
                         }else{
                             location.reload();
                         }
                   }
                  })
             });

         $('#ok_button').click(function(){
          $.ajax({
           url:"ajax-crud/destroy/"+user_id,
           beforeSend:function(){
            $('#ok_button').text('Deleting...');
           },
           success:function(data)
           {
            setTimeout(function(){
             $('#confirmModal').modal('hide');
             $('#user_table').DataTable().ajax.reload();
            }, 2000);
           }
          })
         });
         
        </script>
 </body>
</html>

