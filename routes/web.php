<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::view('/addpost','addpost');
Route::view('/viewpost','viewpost');
Route::get('/ajax_upload', 'AddPostController@index');

Route::post('/ajax_upload/action', 'AddPostController@action')->name('addpost.action');
Route::get('/ajax_upload/get', 'AddPostController@get')->name('viewpost.get');

Route::post('/add_comment/action', 'AddCommentController@action')->name('addcomment.action');

    
Route::post('viewpost/update', 'AddPostController@update')->name('viewpost.update');
Route::get('viewpost/destroy/{id}', 'AddPostController@destroy');